package ru.levelup.alexandr.nam.qa.homework1.task1;

public class Iron extends NonKitchenHouseHold {
    public Iron(int power, String brand, int price, String color) {
        super(power, brand, price, color);
    }

    @Override
    public void plugIn() {

        if (isWorking&&(!isPluged)){
            isPluged = true;
            System.out.println("Continue working");
        }else if(!isWorking){
            isPluged=true;

        }else {
            System.out.println("Nothing changes");
        }



    }

    @Override
    public void turnOn() {

        if (isPluged&&(!isWorking))
        {
            System.out.println("heating ");
            isWorking = true;
        }else
        {
            System.out.println("...Nothing changes");
        }

    }

    @Override
    public void turnOff() {
        super.isWorking = false;
        System.out.println("Stop working");

    }

    @Override
    public void plugOut() {
        super.isPluged = false;
        System.out.println("Stop working");
    }

    public void makeSteam() {
        if (isWorking) {
            System.out.println("Producing steam");
        }
    }
}