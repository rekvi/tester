package ru.levelup.alexandr.nam.qa.homework1.task1;

// Задание с бытовой техникой
public class HouseHoldKeeper {

    public static void main(String[] args) {
        HouseHoldKeeper keeper = new HouseHoldKeeper();
        keeper.startHouseHoldKeeper();

    }

    public void startHouseHoldKeeper() {

        Mixer mx = new Mixer(16, "Samsung", 100, "white");
        Mixer mx2 = new Mixer(16, "Mi", 120, "black");
        Iron ir = new Iron(16, "LG", 121, "black and white");
        Iron ir2 = new Iron(16, "Zarya", 1225, "silver");
        Refregerator ref = new Refregerator(220, "Gefest", 390, "white");
        Refregerator ref2 = new Refregerator(225, "Sony", 347, "red");
        WashMachine wash = new WashMachine(223, "Toshiba", 34, "blue");
        WashMachine wash2 = new WashMachine(223, "Bosh", 34, "black and green");

        //включаем приборы в розетку и запускаем их
        mx.plugIn();
        mx.turnOn();
        mx2.plugIn();
        mx2.turnOn();
        ir.plugIn();
        ir.turnOn();
        ir.makeSteam();

        //проверка логики выключателей
        wash.plugIn();
        wash.setDelayedStart(15);
        wash.turnOn();
        wash.plugIn();
        wash.turnOn();
        wash.plugOut();
        wash.plugIn();
        wash.turnOff();
        wash.turnOn();

        //сортировка по цене всех имеющихся приборов
        Household.printSortedObjects();

        //подсчет потребляемого количества мощности работающих приборов
        Household.countPower();

        //поиск прибора по диапазону цены и цвету на кухне
        KitchenHousehold.findDeviceInKitchen("white", 99, 391);


    }
}
