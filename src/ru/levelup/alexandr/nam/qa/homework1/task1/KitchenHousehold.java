package ru.levelup.alexandr.nam.qa.homework1.task1;

public abstract class KitchenHousehold extends Household {

    public KitchenHousehold(int power, String brand, int price, String color) {
        super(power, brand, price, color);
    }

    public static void findDeviceInKitchen(String color, int minPrice, int maxPrice) //Поиск кухонной техники по дипазону цены
    {
        for (Household i : list) {
            if (i.color.toLowerCase().equals(color.toLowerCase()) && i.price >= minPrice && i.price <= maxPrice) {
                System.out.println(i);
            }
        }

    }
}


