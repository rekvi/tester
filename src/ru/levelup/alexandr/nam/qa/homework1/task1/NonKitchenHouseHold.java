package ru.levelup.alexandr.nam.qa.homework1.task1;

public abstract class NonKitchenHouseHold extends Household {

    final public boolean isHeavy = true;


    public NonKitchenHouseHold(int power, String brand, int price, String color) {
        super(power, brand, price, color);
    }
}
