package ru.levelup.alexandr.nam.qa.homework1.task1;

public class WashMachine extends NonKitchenHouseHold implements Smartable {


    public WashMachine(int power, String brand, int price, String color) {
        super(power, brand, price, color);
    }

    private int min = 0;


    @Override
    public void plugIn() {

        if (isWorking&&(!isPluged)){
            isPluged = true;
            System.out.println("Continue working");
        }else if(!isWorking){
            isPluged=true;

        }else {
            System.out.println("Nothing changes");
        }



    }

    @Override //Класс наследуется от интерфейса Smartable поэтому имеет функцию отложенного старта
    public void turnOn() {

        if (isPluged&&(!isWorking)) {
            if (min > 0) {
                System.out.println("I will start washing in " + min + " minutes");
                isWorking = true;
                min=0;
            } else {
                System.out.println("Washing");
                isWorking = true;
            }
        } else {
            System.out.println("....Nothing changes");
        }

    }

    @Override
    public void turnOff() {
        isWorking = false;
        System.out.println("Stop working");

    }

    @Override
    public void plugOut() {
        isPluged = false;
        System.out.println("Stop working");
    }


    @Override
    public void setDelayedStart(int min) {
        if(isPluged){
            System.out.println("Start timer has been set to "+min+" minutes");
            this.min = min;
        }
        else{
            System.out.println("At first you should plug in ");
        }

    }
}
