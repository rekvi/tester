package ru.levelup.alexandr.nam.qa.homework1.task1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class Household implements Comparable<Household> {
    protected int price;
    private int power;
    private String brand;
    protected String color;
    private static int currentPowerConsume = 0;
    protected static List<Household> list = new ArrayList<>();
    protected boolean isPluged = false;
    protected boolean isWorking = false;


    public Household(int power, String brand, int price, String color) {
        this.power = power;
        this.brand = brand;
        this.price = price;
        this.color = color;
        saveObjToList(this);
    }

    public abstract void plugIn();

    public abstract void turnOn();

    public abstract void turnOff();

    public abstract void plugOut();


    private void saveObjToList(Household obj) {
        list.add(obj);
    }

    //Считает мощность всех включенных в розетку и работающих приборов
    public static void countPower() {
        for (Household i : list) {
            if (i.isPluged || i.isWorking) {
                currentPowerConsume += i.power;
            }
        }
        System.out.println("Power of all working devices is: "+currentPowerConsume);
    }

    @Override
    public String toString() {
        return "Household{" +
                "price=" + price +
                ", power=" + power +
                ", brand='" + brand + '\'' +
                ", color='" + color + '\'' +
                ", isPluged=" + isPluged +
                ", isWorking=" + isWorking +
                '}';
    }

    @Override
    public int compareTo(Household h) {
        return this.price - h.price;
    }

    //Сортировка созданных объектов по цене
    static public void printSortedObjects() {
        Collections.sort(list);
        for (Household i : list) {
            System.out.println(i);
        }
    }
}
